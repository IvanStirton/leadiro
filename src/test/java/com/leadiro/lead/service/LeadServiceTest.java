package com.leadiro.lead.service;

import com.leadiro.lead.application.LeadApplication;
import com.leadiro.lead.entity.Lead;
import com.leadiro.lead.model.LeadRequestModel;
import com.leadiro.lead.model.LeadResponseModel;
import com.leadiro.lead.model.LeadSummaryResponseModel;
import com.leadiro.lead.repository.LeadRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= LeadApplication.class)
@ActiveProfiles("test")
public class LeadServiceTest {

    private static final Long ID = 1L;
    private static final String NAME = "John User";
    private static final String EMAIL = "john@user.com";
    private static final Integer AGE = 31;

    @Autowired
    private LeadService leadService;

    @Autowired
    private LeadRepository leadRepository;

    @Before
    public void setup() {
        leadRepository.deleteAll();
    }

    @Test
    public void addValidLead() {
        LeadRequestModel leadRequestModel = new LeadRequestModel();
        leadRequestModel.setName(NAME);
        leadRequestModel.setEmail(EMAIL);
        leadRequestModel.setAge(AGE);

        LeadResponseModel leadResponseModel = leadService.addLead(leadRequestModel);

        assertNotNull(leadResponseModel.getId());
    }

    @Test
    public void getLeadWithRecordFound() {
        Lead lead = new Lead();
        lead.setName(NAME);
        lead.setEmail(EMAIL);
        lead.setAge(AGE);
        Lead savedLead = leadRepository.save(lead);

        LeadResponseModel leadRecord = leadService.getLead(savedLead.getId());

        assertEquals(leadRecord.getId(), savedLead.getId());
        assertEquals(leadRecord.getName(), NAME);
        assertEquals(leadRecord.getEmail(), EMAIL);
        assertEquals(leadRecord.getAge(), AGE);
    }

    @Test
    public void getLeadWithRecordNotFound() {
        LeadResponseModel leadRecord = leadService.getLead(ID);

        assertNull(leadRecord.getId());
        assertNull(leadRecord.getName());
        assertNull(leadRecord.getEmail());
        assertNull(leadRecord.getAge());
    }

    @Test
    public void deleteLeadSuccessfully() {
        Lead lead = new Lead();
        lead.setName(NAME);
        lead.setEmail(EMAIL);
        lead.setAge(AGE);
        Lead savedLead = leadRepository.save(lead);

        assertEquals(leadRepository.count(), 1);

        leadService.deleteLead(savedLead.getId());

        assertEquals(leadRepository.count(), 0);
    }

    @Test
    public void getLeadSummarySuccessfully() {
        for (int i = 0; i < 3; i++) {
            Lead lead = new Lead();
            lead.setName(NAME);
            lead.setEmail(EMAIL);
            lead.setAge(AGE);
            leadRepository.save(lead);
        }

        LeadSummaryResponseModel summary = leadService.getSummary();

        assertEquals(leadRepository.count(), 3);
        assertEquals(summary.getCount().intValue(), 3);
        assertEquals(summary.getTotalAge().longValue(), AGE * 3);
    }
}
