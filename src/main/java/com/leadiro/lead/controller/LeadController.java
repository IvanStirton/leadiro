package com.leadiro.lead.controller;

import com.leadiro.lead.model.LeadRequestModel;
import com.leadiro.lead.model.LeadResponseModel;
import com.leadiro.lead.model.LeadSummaryResponseModel;
import com.leadiro.lead.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1")
public class LeadController {

    private LeadService leadService;

    @Autowired
    LeadController(LeadService leadService) {
        this.leadService = leadService;
    }

    @RequestMapping(value = "/lead", method = RequestMethod.POST)
    public LeadResponseModel addLead(@RequestBody LeadRequestModel leadRequestModel) {
        return leadService.addLead(leadRequestModel);
    }

    @RequestMapping(value = "/lead/{id}", method = RequestMethod.GET)
    public LeadResponseModel getLead(@PathVariable Long id) {
        return leadService.getLead(id);
    }

    @RequestMapping(value = "/lead/{id}", method = RequestMethod.DELETE)
    public void deleteLead(@PathVariable Long id) {
        leadService.deleteLead(id);
    }

    @RequestMapping(value = "/lead/summary", method = RequestMethod.GET)
    public LeadSummaryResponseModel getSummary() {
        return leadService.getSummary();
    }

}
