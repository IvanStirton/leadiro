package com.leadiro.lead.repository;

import com.leadiro.lead.entity.Lead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface LeadRepository extends JpaRepository<Lead, Long> {

    Optional<Lead> findById(Long id);

    List<Lead> findAll();

    Lead save(Lead lead);

    void delete(Lead lead);
}
