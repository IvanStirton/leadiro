package com.leadiro.lead.dto;

import com.leadiro.lead.entity.Lead;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.List;

public class LeadDTO {

    private Long id;
    private String name;
    private String email;
    private Integer age;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public static LeadDTO fromEntity(Lead lead) {
        LeadDTO leadDTO = new LeadDTO();
        leadDTO.setId(lead.getId());
        leadDTO.setName(lead.getName());
        leadDTO.setEmail(lead.getEmail());
        leadDTO.setAge(lead.getAge());
        return leadDTO;
    }

    public static List<LeadDTO> fromEntities(List<Lead> leads) {
        List<LeadDTO> leadDTOList = new ArrayList<>();
        for (Lead lead: leads) {
            leadDTOList.add(fromEntity(lead));
        }
        return leadDTOList;
    }
}
