package com.leadiro.lead.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {
		"com.leadiro.lead.controller",
		"com.leadiro.lead.service"
})
@EntityScan(basePackages = {
		"com.leadiro.lead.entity"
})
@EnableJpaRepositories(basePackages = {
		"com.leadiro.lead.repository"
})
public class LeadApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeadApplication.class, args);
	}

}

