package com.leadiro.lead.service;

import com.leadiro.lead.model.LeadRequestModel;
import com.leadiro.lead.model.LeadResponseModel;
import com.leadiro.lead.model.LeadSummaryResponseModel;
import org.springframework.stereotype.Service;

@Service
public interface LeadService {

    /**
     * Add a lead to the database
     *
     * @param leadRequestModel leadRequestModel
     * @return LeadResponseModel
     */
    LeadResponseModel addLead(LeadRequestModel leadRequestModel);


    /**
     * Get a lead from the database
     * @param id - The ID of the lead
     * @return LeadResponseModel
     */
    LeadResponseModel getLead(Long id);

    /**
     * Delete a lead from the database
     * @param id - The ID of the lead
     */
    void deleteLead(Long id);

    /**
     * Get a summary of all leads in the database with aggregate functions
     * @return LeadSummaryResponseModel
     */
    LeadSummaryResponseModel getSummary();
}
