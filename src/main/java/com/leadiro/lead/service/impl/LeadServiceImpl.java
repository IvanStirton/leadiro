package com.leadiro.lead.service.impl;

import com.leadiro.lead.dto.LeadDTO;
import com.leadiro.lead.entity.Lead;
import com.leadiro.lead.model.LeadRequestModel;
import com.leadiro.lead.model.LeadResponseModel;
import com.leadiro.lead.model.LeadSummaryResponseModel;
import com.leadiro.lead.repository.LeadRepository;
import com.leadiro.lead.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class LeadServiceImpl implements LeadService {

    private LeadRepository leadRepository;

    @Autowired
    LeadServiceImpl(LeadRepository leadRepository) {
        this.leadRepository = leadRepository;
    }

    @Override
    public LeadResponseModel addLead(LeadRequestModel leadRequestModel) {
        LeadResponseModel leadResponseModel = new LeadResponseModel();

        Lead lead = new Lead();
        lead.setName(leadRequestModel.getName());
        lead.setEmail(leadRequestModel.getEmail());
        lead.setAge(leadRequestModel.getAge());

        Lead savedLead = leadRepository.save(lead);
        if (savedLead != null){
            LeadDTO leadDTO = LeadDTO.fromEntity(savedLead);
            leadResponseModel.setId(leadDTO.getId());
        }
        return leadResponseModel;
    }

    @Override
    public LeadResponseModel getLead(Long id) {
        LeadResponseModel leadResponseModel = new LeadResponseModel();
        Optional<Lead> lead = leadRepository.findById(id);
        if (lead.isPresent()) {
            LeadDTO leadDTO = LeadDTO.fromEntity(lead.get());
            leadResponseModel.setId(leadDTO.getId());
            leadResponseModel.setName(leadDTO.getName());
            leadResponseModel.setEmail(leadDTO.getEmail());
            leadResponseModel.setAge(leadDTO.getAge());
        }
        return leadResponseModel;
    }

    @Override
    public void deleteLead(Long id) {
        Optional<Lead> lead = leadRepository.findById(id);
        lead.ifPresent(presentLead -> leadRepository.delete(presentLead));
    }

    @Override
    public LeadSummaryResponseModel getSummary() {
        LeadSummaryResponseModel leadSummaryResponseModel = new LeadSummaryResponseModel();
        List<Lead> leadEntities = leadRepository.findAll();
        if (leadEntities != null){
            List<LeadDTO> leads = LeadDTO.fromEntities(leadEntities);
            leadSummaryResponseModel.setCount(leads.size());
            leadSummaryResponseModel.setTotalAge(leads.stream().mapToInt(LeadDTO::getAge).sum());
        } else {
            leadSummaryResponseModel.setCount(0);
            leadSummaryResponseModel.setTotalAge(0);
        }
        return leadSummaryResponseModel;
    }

}
